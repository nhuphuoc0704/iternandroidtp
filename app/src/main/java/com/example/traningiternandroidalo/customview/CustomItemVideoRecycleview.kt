package com.example.traningiternandroidalo.customview

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.traningiternandroidalo.R
import com.example.traningiternandroidalo.databinding.CustomItemVideoRecycleviewBinding
import com.example.traningiternandroidalo.model.WallpaperResponse
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File


class CustomItemVideoRecycleview(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {
    private var binding: CustomItemVideoRecycleviewBinding
    private val simpleCache: SimpleCache by lazy {
        VideoCache.getInstance(context)
    }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_item_video_recycleview, this, true)
        binding = CustomItemVideoRecycleviewBinding.inflate(inflater, this, true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.exoPlayerView.clipToOutline = true
        }
    }

    fun text(name: String) {
        binding.tvNameVideo.text = name
    }

    fun video(wallpaper: WallpaperResponse.Data.Wallpaper) {
        setSizeplayerView()

        val animation = AnimationUtils.loadAnimation(context, R.anim.fade_out)
        try {
            val exoPlayer: SimpleExoPlayer
            val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()

            val trackSelector: TrackSelector =
                DefaultTrackSelector(AdaptiveTrackSelection.Factory(bandwidthMeter))

            exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
            val dataSourceFactory = DefaultHttpDataSourceFactory("exoplayer_video")

            val extractorsFactory: ExtractorsFactory = DefaultExtractorsFactory()
            val cacheDataSourceFactory = CacheDataSourceFactory(simpleCache, dataSourceFactory)

            val videouri: Uri = Uri.parse(wallpaper.originUrlString())
            val mediaSource =
                ExtractorMediaSource.Factory(cacheDataSourceFactory).createMediaSource(videouri)

            binding.exoPlayerView.setPlayer(exoPlayer)
            exoPlayer.prepare(mediaSource)
            exoPlayer.repeatMode = Player.REPEAT_MODE_ONE
            exoPlayer.addListener(object : Player.EventListener {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    super.onPlayerStateChanged(playWhenReady, playbackState)
                    if (playbackState == Player.STATE_READY) {
                        binding.imgIconVideo.startAnimation(animation!!)
                        Handler().postDelayed({
                            binding.imgIconVideo.visibility = View.GONE
                        }, 1000)
                        exoPlayer.setPlayWhenReady(true)

                    }
                }
            })
        } catch (e: Exception) {
            Log.d("EXOPLAYER_EXCEPTION", "${e.message}")
        }

    }

    object VideoCache {
        private var sDownloadCache: SimpleCache? = null
        private const val maxCacheSize: Long = 100 * 1024 * 1024

        fun getInstance(context: Context): SimpleCache {
            val evictor = LeastRecentlyUsedCacheEvictor(maxCacheSize)
            if (sDownloadCache == null) sDownloadCache =
                SimpleCache(File(context.cacheDir, "video-wallpaper"), evictor)
            return sDownloadCache as SimpleCache
        }
    }

    fun setSizeplayerView() {
        val displaymetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)
        val deviceheight = displaymetrics.heightPixels / 2
        binding.exoPlayerView.layoutParams.height = deviceheight.toInt()
    }
}