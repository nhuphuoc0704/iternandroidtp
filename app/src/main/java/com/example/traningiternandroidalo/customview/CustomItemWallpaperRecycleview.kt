package com.example.traningiternandroidalo.customview

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.example.traningiternandroidalo.R
import com.example.traningiternandroidalo.databinding.CutsomItemWallpaperRecycleviewBinding
import com.example.traningiternandroidalo.model.WallpaperResponse.Data.Wallpaper


class CustomItemWallpaperRecycleview(context: Context, attrs: AttributeSet?) : ConstraintLayout(
    context,
    attrs
) {
    private lateinit var binding: CutsomItemWallpaperRecycleviewBinding


    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.cutsom_item_wallpaper_recycleview, this, true)
        binding= CutsomItemWallpaperRecycleviewBinding.inflate(inflater,this,true)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            binding.imgWallpaperCustomView.clipToOutline = true
        }

    }


    fun setText(text: String) {
        binding.tvNameWallpaperCustomView.text = text
    }

    fun setImageWallpaper(wallpaper: Wallpaper) {
        setSizeWallpaper(context,wallpaper)
        if(wallpaper.category!=null){

            Glide.with(context)
               .asBitmap()
                .load(wallpaper.originUrlString())
                .transition(withCrossFade())
                .into(binding.imgWallpaperCustomView)

        }
        else {
            Glide.with(context)
                .asBitmap()
                .load(wallpaper.originUrlString())
                .onlyRetrieveFromCache(true)
                .transition(withCrossFade())
                .into(binding.imgWallpaperCustomView)
        }


    }



    fun setSizeWallpaper(context: Context, wallpaper: Wallpaper) {
        val displaymetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)
        val deviceheight = displaymetrics.heightPixels / 2
        //val wallpaperRatio = getWallpaperRatio(wallpaper)
        val imageWidth = binding.imgWallpaperCustomView.width
        binding.imgWallpaperCustomView.layoutParams.height = deviceheight
           //(imageWidth / wallpaperRatio).toInt()

    }

//    private fun getWallpaperRatio(wallpaper: Wallpaper): Double {
//        val strSize = wallpaper.imgSize
//        val spilitStrSize = strSize?.split("x")
//        val width = spilitStrSize?.get(0)?.toDouble()
//        val height = spilitStrSize?.get(1)?.toDouble()
//        return width!! / height!!
//
//
//    }

}