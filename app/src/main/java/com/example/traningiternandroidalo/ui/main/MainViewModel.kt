package com.example.traningiternandroidalo.ui.main

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.traningiternandroidalo.model.VideoWallpaperResponse
import com.example.traningiternandroidalo.model.WallpaperResponse
import com.example.traningiternandroidalo.model.WallpaperResponse.Data.Wallpaper
import com.example.traningiternandroidalo.network.CallApiInterface
import com.example.traningiternandroidalo.realm.WallpaperLocal
import com.example.traningiternandroidalo.sqlite.SQLiteHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    private val _data = MutableLiveData<List<Wallpaper>>()

    fun getData(): LiveData<List<Wallpaper>> {
        return _data
    }

    fun callApi(context : Context) {
        //val db = SQLiteHelper(context)
        val apiInterface = CallApiInterface.create().getData("vi", "VN", 1)
        apiInterface.enqueue(object : Callback<WallpaperResponse> {
            override fun onResponse(call: Call<WallpaperResponse>, response: Response<WallpaperResponse>) {
                Log.d("ON_RESPONSE", "success: $response")
                _data.postValue(response.body()!!.data.wallpapers)


            }

            override fun onFailure(call: Call<WallpaperResponse>, t: Throwable) {
                Log.d("ON_RESPONSE", "error: $t")


            }


        })
    }

    fun callApiVideo(context : Context){
        val callApiInterface = CallApiInterface.create().getVideoData("vi","VN",0,10)
        callApiInterface.enqueue(object : Callback<VideoWallpaperResponse>{
            override fun onResponse(
                call: Call<VideoWallpaperResponse>,
                response: Response<VideoWallpaperResponse>
            ) {

                    Log.d("ON_RESPONSE", "success: $response")
                    _data.postValue(response.body()!!.data)
                WallpaperLocal.insertData(response.body()!!.data)

            }
            override fun onFailure(call: Call<VideoWallpaperResponse>, t: Throwable) {
                Log.d("ON_RESPONSE", "success: $t")
                val postValue= WallpaperLocal.getWallpaperLocal()
                _data.postValue(postValue)
            }

        })

    }
    fun callApiCategory(context : Context){
        val callApiInterface = CallApiInterface.create().getCategoryWallpaper("vi","VN",183,0,10)
        callApiInterface.enqueue(object : Callback<WallpaperResponse>{
            override fun onResponse(
                call: Call<WallpaperResponse>,
                response: Response<WallpaperResponse>
            ) {

                    Log.d("ON_RESPONSE", "success: $response")
                    _data.postValue(response.body()!!.data.wallpapers)
                WallpaperLocal.insertData(response.body()!!.data.wallpapers)

            }
            override fun onFailure(call: Call<WallpaperResponse>, t: Throwable) {
                Log.d("ON_RESPONSE", "success: $t")
                val postValue= WallpaperLocal.getWallpaperLocal()
                _data.postValue(postValue)
            }

        })

    }
}



