package com.example.traningiternandroidalo.ui.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.traningiternandroidalo.DaggerActivity
import com.example.traningiternandroidalo.adapter.MainAdapter
import com.example.traningiternandroidalo.databinding.ActivityMainBinding
import com.example.traningiternandroidalo.model.WallpaperResponse.Data.Wallpaper
import com.example.traningiternandroidalo.utils.RecyclerViewScrollListener


class MainActivity : AppCompatActivity() {
    private val sharedName = "SharedPreferencesName"
    lateinit var viewModel: MainViewModel
    lateinit var binding: ActivityMainBinding
    lateinit var mainAdapter: MainAdapter
    var wallpapers: ArrayList<Wallpaper> = ArrayList()
    lateinit var  scrollListener: RecyclerViewScrollListener
    val callbackAdapter = object : MainAdapter.IOnClickItem {
        override fun onClickImage(position: Int) {
            Log.d("CHECK_CLICK_ITEM", "Position click is: $position")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.callApiCategory(this)
        scrollListener()
        setupAdapter()
        setUpObServer()

    }

    private fun setUpObServer() {
        viewModel.getData().observe(this, object : Observer<List<Wallpaper>> {
            override fun onChanged(t: List<Wallpaper>?) {
                if(t!=null){
                    wallpapers.addAll(t)
                    mainAdapter.notifyDataSetChanged()
                }

            }


        })

    }


}