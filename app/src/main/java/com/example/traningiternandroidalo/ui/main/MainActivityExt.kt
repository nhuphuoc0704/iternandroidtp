package com.example.traningiternandroidalo.ui.main

import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.traningiternandroidalo.adapter.MainAdapter
import com.example.traningiternandroidalo.utils.RecyclerViewScrollListener

fun MainActivity.setupAdapter() {
    binding.recycleView.layoutManager = GridLayoutManager(this,2 , LinearLayoutManager.VERTICAL,false)
    mainAdapter = MainAdapter(this,callbackAdapter, wallpapers)
    binding.recycleView.adapter = mainAdapter
    binding.recycleView.addOnScrollListener(scrollListener)
    binding.recycleView.isNestedScrollingEnabled= false
}
fun MainActivity.scrollListener(){
    scrollListener= object : RecyclerViewScrollListener(){
        override fun onVisibleItemCount(visibleItemCount: Int) {
            Log.d("visible item", "count item $visibleItemCount")
        }

        override fun onItemIsFirstVisibleItem(index: Int) {
            Log.d("visible item", "first index "+ index.toString())
        }

    }
}