package com.example.traningiternandroidalo.component

import com.example.traningiternandroidalo.DaggerActivity
import com.example.traningiternandroidalo.realm.MyRealmApp
import dagger.Component


@Component
interface MagicBox {
    fun inject(app: DaggerActivity)
    fun inject(myRealmApp: MyRealmApp)
}