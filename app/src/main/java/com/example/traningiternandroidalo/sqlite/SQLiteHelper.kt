package com.example.traningiternandroidalo.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.traningiternandroidalo.model.WallpaperResponse.Data.Wallpaper


class SQLiteHelper(context: Context) : SQLiteOpenHelper(
    context,
    DATABASE_NAME,
    null,
    DATABASE_VERSION
) {
    val thisContext = context
    companion object{
        private val DATABASE_NAME="database"
        private val DATABASE_VERSION=17 //13:47
        private val TABLE_WALLPAPER="tblwallpaper"
        private val KEY_ID="id"
        private val KEY_ID_WALLPAPER="idWallpaper"
        private val KEY_NAME="name"
        private val KEY_IS_VIDEO="isVideo"
        private val KEY_URL="url"
        private val KEY_WALLPAPER="wallpaper"

    }
    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_WALLPAPER_TABLE = ("CREATE TABLE " + TABLE_WALLPAPER + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID_WALLPAPER + " INTEGER ,"
                + KEY_NAME + " TEXT,"
                + KEY_IS_VIDEO +" INTEGER,"
                + KEY_URL +" TEXT"
                + ")")
        db?.execSQL(CREATE_WALLPAPER_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_WALLPAPER)
        onCreate(db)
    }

    fun insertDataResponse(wallpapers: List<Wallpaper>){
        val db = this.writableDatabase
        val contentValues = ContentValues()
        for(wallpaper in wallpapers){
            contentValues.put(KEY_ID_WALLPAPER, wallpaper.id)
            contentValues.put(KEY_NAME, wallpaper.name)
            contentValues.put(KEY_IS_VIDEO, if (wallpaper.isVideo!!) 1 else 0)
            contentValues.put(KEY_URL, wallpaper.url)
            db.insert(TABLE_WALLPAPER,null,contentValues)
        }

        db.close()
    }
    fun getWallpaperLocal() : List<Wallpaper>{
        val db = this.readableDatabase
        var results : ArrayList<Wallpaper> = ArrayList<Wallpaper>()
        val query = "SELECT * FROM $TABLE_WALLPAPER"
        var cursor : Cursor? =null
        try {
            cursor= db.rawQuery(query, null)
        }catch (e: SQLiteException){
            db.execSQL(query)
            return ArrayList()
        }
        var idWallpaper :Int
        var name : String
        var url : String
        var isVideo: Int
        while(cursor.moveToNext()){
            idWallpaper= cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID_WALLPAPER))
                name = cursor.getString(cursor.getColumnIndexOrThrow(KEY_NAME))
                isVideo = cursor.getInt(cursor.getColumnIndexOrThrow(KEY_IS_VIDEO))
                url=cursor.getString(cursor.getColumnIndexOrThrow(KEY_URL))
                var wallpaper =Wallpaper()
                wallpaper.isVideo= isVideo==1
                wallpaper.name= name
                wallpaper.id= idWallpaper
                wallpaper.url=url
                results.add(wallpaper)
        }

        return results
    }





}