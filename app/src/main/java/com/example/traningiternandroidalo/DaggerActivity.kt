package com.example.traningiternandroidalo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.traningiternandroidalo.component.DaggerMagicBox
import com.example.traningiternandroidalo.model.AppRealmManager
import javax.inject.Inject

class DaggerActivity : AppCompatActivity() {
    @Inject
    lateinit var info: AppRealmManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMagicBox.create().inject(this)
    }
}


