package com.example.traningiternandroidalo.realm

import android.util.Log
import androidx.room.PrimaryKey
import com.example.traningiternandroidalo.model.WallpaperResponse
import io.realm.Realm
import io.realm.RealmObject


open class WallpaperLocal() : RealmObject() {
     @PrimaryKey
     var idWallpaper : Int =0
    var name: String=""
    var url :String =""
    var isVideo : Int =0

    companion object{
        fun insertData(wallpapers: List<WallpaperResponse.Data.Wallpaper>){
            var realm: Realm = Realm.getDefaultInstance()
            deleteAll()
            realm.executeTransaction { realm ->
                var insertData = ArrayList<WallpaperLocal>()
                for(item in wallpapers){
                    var wallpaperLocal : WallpaperLocal = realm.createObject(WallpaperLocal::class.java)
                    wallpaperLocal.idWallpaper= item.id!!
                    wallpaperLocal.isVideo= if(item.isVideo!!) 1 else 0
                    wallpaperLocal.url= item.url!!
                    wallpaperLocal.name= item.name!!
                    insertData.add(wallpaperLocal)
                }
                realm.insert(insertData)
            }
            Log.d("REALM_INSERT_RESPONSE","$wallpapers.size")

        }

        private fun deleteAll() {
            var realm : Realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            realm.delete(WallpaperLocal::class.java)
            realm.commitTransaction()
        }

        fun getWallpaperLocal(): List<WallpaperResponse.Data.Wallpaper>{
            var realm: Realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            var results : ArrayList<WallpaperResponse.Data.Wallpaper> = ArrayList<WallpaperResponse.Data.Wallpaper>()
            var localDatas: List<WallpaperLocal> = realm.where(WallpaperLocal::class.java).findAll()
            Log.d("LOCAL_DATA_SIZE", "${localDatas.size}")
            for(item in localDatas){
                var wallpaper = WallpaperResponse.Data.Wallpaper()
                wallpaper.url= item.url
                wallpaper.name= item.name
                wallpaper.id= item.idWallpaper
                wallpaper.isVideo= item.isVideo==1
                results.add(wallpaper)
            }
            return results

        }

    }

}

