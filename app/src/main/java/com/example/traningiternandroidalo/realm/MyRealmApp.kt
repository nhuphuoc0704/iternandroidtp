package com.example.traningiternandroidalo.realm

import android.app.Application
import com.example.traningiternandroidalo.component.DaggerMagicBox
import com.example.traningiternandroidalo.model.AppRealmManager
import dagger.Component
import io.realm.Realm
import io.realm.RealmConfiguration

class MyRealmApp   : Application() {


    override fun onCreate() {
        super.onCreate()
        Realm.init(applicationContext)
        val configuration = RealmConfiguration.Builder()
            .allowWritesOnUiThread(true)
            .name("realm.db")
            .schemaVersion(6)
            .build()
        Realm.setDefaultConfiguration(configuration)
    }
}