package com.example.traningiternandroidalo.network

import com.example.traningiternandroidalo.model.VideoWallpaperResponse
import com.example.traningiternandroidalo.model.WallpaperResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface CallApiInterface {

    @GET("api/wallpapers/images/only")
    fun getData(
        @Header("language") language: String?,
        @Header("country") country: String?,
        @Query("imagePageNumber") imagePageNumber: Int?
    ): Call<WallpaperResponse>

    @GET("api/wallpapers/video")
    fun getVideoData(
        @Header("language") language: String?,
        @Header("country") country: String?,
        @Query("offset") offset: Int?,
        @Query("limit") limit: Int?
    ): Call<VideoWallpaperResponse>

    @GET("api/wallpapers/{categoryId}/category")
    fun getCategoryWallpaper(
        @Header("language") language: String?,
        @Header("country") country: String?,
        @Path("categoryId") categoryId: Long?,
        @Query("offset") offset: Int?,
        @Query("limit") limit: Int?

    ): Call<WallpaperResponse>


    companion object {
        private val BASE_URL = "https://dev.wallpapers-free.com/"
        fun create(): CallApiInterface {
            val gson: Gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(CallApiInterface::class.java)


        }
    }
}