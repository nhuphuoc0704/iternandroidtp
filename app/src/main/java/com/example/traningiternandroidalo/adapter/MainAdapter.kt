package com.example.traningiternandroidalo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.traningiternandroidalo.databinding.ItemImageRecycleviewBinding
import com.example.traningiternandroidalo.databinding.ItemVideoRecycleviewBinding
import com.example.traningiternandroidalo.model.WallpaperResponse.Data.Wallpaper

class MainAdapter(val context : Context, val callBack: IOnClickItem, private var mListWallpaper: ArrayList<Wallpaper>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private final val VIEW_TYPE_IMAGE = 1
    private final val VIEW_TYPE_VIDEO = 2


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_IMAGE) {
            ImageViewHolder(ItemImageRecycleviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        } else {
            VideoViewHolder(ItemVideoRecycleviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }


    override fun getItemCount(): Int {
        return mListWallpaper.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var wallpaper = mListWallpaper[position]
        if (getItemViewType(position) == VIEW_TYPE_IMAGE) {
            (holder as ImageViewHolder).bind(wallpaper, position)

        } else (holder as VideoViewHolder).bind(wallpaper,position)
    }


    override fun getItemViewType(position: Int): Int {
        return if ( mListWallpaper[position].isVideo!!) {
            VIEW_TYPE_VIDEO
        } else VIEW_TYPE_IMAGE

        return super.getItemViewType(position)
    }

    inner class ImageViewHolder(private val binding: ItemImageRecycleviewBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(wallpaper: Wallpaper, position: Int)= binding.apply {
            customView.setText(wallpaper.name!!)
            customView.setImageWallpaper(wallpaper)

            root.setOnClickListener {
                callBack.onClickImage(position)
            }
        }


    }

    inner class VideoViewHolder(private val binding: ItemVideoRecycleviewBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(wallpaper: Wallpaper, position: Int)= binding.apply {
            customViewVideoItem.text(wallpaper.name!!)
            customViewVideoItem.video(wallpaper)

        }
    }


    interface IOnClickItem {
        fun onClickImage(position: Int)
    }
}


