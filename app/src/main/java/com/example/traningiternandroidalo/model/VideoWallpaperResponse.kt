package com.example.traningiternandroidalo.model

import java.io.Serializable

data class VideoWallpaperResponse(
    val data: List<WallpaperResponse.Data.Wallpaper>,
    val status: WallpaperResponse.Status
): Serializable