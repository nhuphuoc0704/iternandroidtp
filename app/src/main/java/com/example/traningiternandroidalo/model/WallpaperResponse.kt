package com.example.traningiternandroidalo.model


import com.google.gson.annotations.SerializedName


data class WallpaperResponse(
    @SerializedName("data")
    var `data`: Data,
    @SerializedName("status")
    var status: Status
) {
    data class Data(
        @SerializedName("nextImagePageNumber")
        var nextImagePageNumber: Int,
        @SerializedName("nextVideoPageNumber")
        var nextVideoPageNumber: Int,
        @SerializedName("relateHashtags")
        var relateHashtags: List<RelateHashtag>,
        @SerializedName("wallpapers")
        var wallpapers: List<Wallpaper>
    ) {
        data class RelateHashtag(
            @SerializedName("createdDate")
            var createdDate: Long,
            @SerializedName("description")
            var description: Any,
            @SerializedName("displayByLang")
            var displayByLang: String,
            @SerializedName("hashtag")
            var hashtag: String,
            @SerializedName("id")
            var id: Long,
            @SerializedName("isShow")
            var isShow: String,
            @SerializedName("isVideo")
            var isVideo: Boolean,
            @SerializedName("name")
            var name: String,
            @SerializedName("searchName")
            var searchName: String,
            @SerializedName("url")
            var url: String,
            @SerializedName("wallCount")
            var wallCount: Int
        )


        class Wallpaper {
            @SerializedName("category")
            val category: String? = null

            @SerializedName("count")
            val count: Int? = null

            @SerializedName("countByCountry")
            val countByCountry: String? = null

            @SerializedName("country")
            val country: String? = null

            @SerializedName("defaultCountry")
            val defaultCountry: String? = null

            @SerializedName("downDate")
            val downDate: Long? = null

            @SerializedName("hashTag")
            val hashTag: String? = null

            @SerializedName("id")
            var id: Int? = null

            @SerializedName("imgSize")
            val imgSize: String? = null

            @SerializedName("isApproved")
            val isApproved: String? = null

            @SerializedName("isVideo")
            var isVideo: Boolean? = null

            @SerializedName("lastPlayDate")
            val lastPlayDate: Long? = null

            @SerializedName("level")
            val level: Int? = null

            @SerializedName("like")
            val like: Int? = null

            @SerializedName("name")
            var name: String? = null

            @SerializedName("owner")
            val owner: String? = null

            @SerializedName("playCount")
            val playCount: Int? = null

            @SerializedName("screenRatio")
            val screenRatio: String? = null

            @SerializedName("searchName")
            val searchName: String? = null

            @SerializedName("unlike")
            val unlike: Int? = null

            @SerializedName("url")
            var url: String? = null

            fun getFileName(url: String): String {
                val subString = url.split("/")
                if (subString.isNotEmpty()) return subString[subString.size - 1]
                return url
            }

            fun minThumbURLString(): String {
                return if (isVideo == true) {
                    val fileName = getFileName(url ?: "")
                    val lastPath = "thumbs/" + fileName.replace(".mp4", ".jpg")
                    val path = url?.replace(fileName, lastPath)
                    "https://laptop.ahaa.com.vn/video7storage/$path"
                } else {
                    "https://laptop.ahaa.com.vn/wall7storage/minthumbnails/$url"
                }
            }

            fun thumbUrlString(): String {
                return if (isVideo == true) {
                    val fileName = getFileName(url ?: "")
                    val lastPath = "thumbs/$fileName"
                    val path = url?.replace(fileName, lastPath)
                    "https://laptop.ahaa.com.vn/video7storage/$path"
                } else {
                    "https://laptop.ahaa.com.vn/wall7storage/thumbnails/$url"
                }
            }

            fun originUrlString(): String {
                return if (isVideo == true) {
                    "https://laptop.ahaa.com.vn/video7storage/$url"
                } else {
                    "https://laptop.ahaa.com.vn/wall7storage/$url"
                }
            }
        }
    }

    data class Status(
        @SerializedName("message")
        var message: String,
        @SerializedName("statusCode")
        var statusCode: Int
    )
}