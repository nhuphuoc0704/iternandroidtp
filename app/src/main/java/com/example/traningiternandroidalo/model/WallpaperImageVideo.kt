package com.example.traningiternandroidalo.model

import com.google.gson.annotations.SerializedName

data class WallpaperImageVideo  (@SerializedName("createDate")
                                 val createdDate:Int =0,
                                 @SerializedName("id")
                                 val id: Int =0,
                                 @SerializedName("name")
                                 val name : String ="",
                                 @SerializedName("hashTag")
                                 val hashTag : String="",
                                 @SerializedName("searchName")
                                 val searchName : String="",
                                 @SerializedName("country")
                                 val country : String="",
                                 @SerializedName("category")
                                 val category : String="",
                                 @SerializedName("url")
                                 val url : String="",
                                 @SerializedName("screenRatio")
                                 val screenRatio : String="",
                                 @SerializedName("imgSize")
                                 val imgSize : String="",
                                 @SerializedName("countByCountry")
                                 val countByCountry : String="",
                                 @SerializedName("playCount")
                                 val playCount : Int=0,
                                 @SerializedName("level")
                                 val level : Int=0,
                                 @SerializedName("defaultCountry")
                                 val defaultCountry : String="",
                                 @SerializedName("like")
                                 val like : Int=0,
                                 @SerializedName("unLike")
                                 val unLike : Int=0,
                                 @SerializedName("isApproved")
                                 val isApproved : String="",
                                 @SerializedName("lastPlayDate")
                                 val lastPlayDate : Int=0,
                                 @SerializedName("downDate")
                                 val downDate : Int=0,
                                 @SerializedName("isVideo")
                                 val isVideo : Boolean=false,
                                 @SerializedName("type")
                                 val type : String="") {

}
